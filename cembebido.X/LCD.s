.equ __30F4013, 1
.include "p30F4013.inc"

	
.GLOBAL _funcion1
.GLOBAL _funcion2
.GLOBAL _funcion3
.GLOBAL _funcion4
.GLOBAL _var
.GLOBAL _comandoLCD ;done
.GLOBAL _datoLCD ;done
.GLOBAL _busyflagLCD ;done
.GLOBAL _iniLCD8bits;done  
.GLOBAL _RETARDO_15ms; done
.GLOBAL _imprimeLCD; done
.EQU    RS_LCD, RD0
.EQU	RW_LCD,	RD1
.EQU	E_LCD,	RD2
    
    
    
    
    
    
_imprimeLCD:
    push w1 ;se respaldan datos de w1
    push w3 ;se respaldan datos de w3
    clr w3
    mov w0, w3 ;se mueve la direccion recibida de la funcion de C a w3
    clr w0 ;se limpia w0 para poder usarlo despues como argumento de funcion
    cicloimp:
	mov.b [w3++], w1;en w0 se envia la direccion del primer elemento del vector
	cp0.b w1 
	bra z,finimp
	MOV w1,w0
	call _busyflagLCD
	call _datoLCD
	goto cicloimp
    
finimp:

    pop w1
    pop w3
    return
    
    
_datoLCD:
    BSET PORTD, #RS_LCD
    NOP    
    BCLR PORTD, #RW_LCD
    NOP
    BSET PORTD, #E_LCD
    NOP 
    MOV.B WREG, PORTB  
    NOP
    BCLR PORTD, #E_LCD
    NOP

    
    return

_busyflagLCD:
    SETM TRISB; Se configura PORTB como entrada
    NOP
    BCLR PORTD, #RS_LCD;
    NOP
    BSET PORTD, #RW_LCD;se pone RW en alto para hacer lectura de registro LCD
    NOP
    BSET PORTD, #E_LCD;
    CHECAR:
    BTSC PORTB, #RB7; se verifica si busy flag esta en 0
    GOTO CHECAR ;BRA NZ,CHECAR; si no esta en cero significa que aun esta ocupado
    CLR TRISB; se vuelve a poner PORTB como salida
    BCLR PORTD, #RW_LCD
    NOP

    return
    
_iniLCD8bits:
    CALL _RETARDO_15ms
    MOV #0x30, W0
    CALL   _comandoLCD

    CALL _RETARDO_15ms
    MOV #0x30, W0
    CALL   _comandoLCD

    CALL _RETARDO_15ms
    MOV #0x30, W0
    CALL   _comandoLCD

    CALL _busyflagLCD
    ;CALL _RETARDO_15ms
    MOV #0x38, W0
    CALL   _comandoLCD
    
    CALL _busyflagLCD
    ;CALL _RETARDO_15ms
    MOV #0x08, W0
    CALL   _comandoLCD

    CALL _busyflagLCD
    ;CALL _RETARDO_15ms
    MOV #0x01, W0
    CALL   _comandoLCD    
    ;CALL _RETARDO_15ms
    CALL _busyflagLCD
    MOV #0x06, W0
    CALL   _comandoLCD    
    
    ;CALL _RETARDO_15ms
    CALL _busyflagLCD
    MOV #0x0F, W0
    CALL   _comandoLCD    
    ;done
    ;CALL _RETARDO_15ms
    CALL _busyflagLCD
    
    return
    
_comandoLCD:
    BCLR PORTD, #RS_LCD
    NOP
    BCLR PORTD, #RW_LCD
    NOP
    BSET PORTD, #E_LCD
    NOP
    MOV.B WREG, PORTB ; MOV.B W0, PORTB
    NOP
    BCLR PORTD, #E_LCD
    NOP
    
    return
    
_RETARDO_15ms:
    PUSH W0
    CLR W0
    MOV #0x24E1,W0
CICLO1:
    DEC W0,W0
    BRA NZ,CICLO1

    POP W0
    RETURN
    
    
    
    


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
_funcion1:
    push w0
    mov #3,w0
    mov w0, _var
    pop w0
    return

_funcion2:
    push w1
    mov #12, w0
    mov #3, w1
    add w0,w1,w0
    pop w1
    return  
    
_funcion3:
    add w0,w1,w0 ;w0 y w1 ya estan cargados con los argumentos enviados de c
    return  
_funcion4:
    push w1
    push w2
    
    clr w2
    ciclo:
	mov.b [w0++], w1;en w0 se envia la direccion del primer elemento del vector
	cp0.b w1
	bra z,fin
	inc w2,w2
	goto ciclo
    
fin:
    mov w2,w0
    pop w2
    pop w1
    return
    

    

.include "p30F4013.inc"
.GLOBAL _comandoLCD ;done
.GLOBAL _datoLCD ;done
.GLOBAL _busyflagLCD ;done
.GLOBAL _iniLCD8bits;done  
.GLOBAL _RETARDO_15ms; done
.GLOBAL _imprimeLCD; done
.GLOBAL _candado; done
.EQU    RS_LCD, RD0
.EQU	RW_LCD,	RD1
.EQU	E_LCD,	RD2


_candado:
    PUSH W0
    PUSH W1
    PUSH W2
    MOV #OSCCONL,W0
    MOV #0x46,W1
    MOV #0x57,W2
    MOV.B W1,[W0]
    MOV.B W2,[W0]
    BSET OSCCONL,#LPOSCEN
    POP W0
    POP W1
    POP W2
    
    return
    
_imprimeLCD:
    push w1  ;se respaldan datos de w1
    push w3 ;se respaldan datos de w3
    clr w3
    mov w0, w3 ;se mueve la direccion recibida de la funcion de C a w3
    clr w0 ;se limpia w0 para poder usarlo despues como argumento de funcion
    cicloimp:
	mov.b [w3++], w1;en w3 se tiene la direccion del primer elemento del vector
	cp0.b w1 ;se compara si no se ha llegado al final del vector
	bra z,finimp ;si se levanta la bandera zero se finaliza el ciclo
	MOV w1,w0 ;se mueve valor de vector a wreg para que se ingrese como dato del LCD
	call _busyflagLCD
	call _datoLCD
	goto cicloimp
    
finimp:

    pop w1
    pop w3
    return
    
    

    
_datoLCD:
    BSET PORTD, #RS_LCD
    NOP    
    BCLR PORTD, #RW_LCD
    NOP
    BSET PORTD, #E_LCD
    NOP 
    MOV.B WREG, PORTB ; MOV.B WREG0, PORTB 
    NOP
    BCLR PORTD, #E_LCD
    NOP
    
    return

_busyflagLCD:
    SETM TRISB; Se configura PORTB como entrada
    NOP
    BCLR PORTD, #RS_LCD;
    NOP
    BSET PORTD, #RW_LCD;se pone RW en alto para hacer lectura de registro LCD
    NOP
    BSET PORTD, #E_LCD;
    CHECAR:
    BTSC PORTB, #RB7; se verifica si busy flag esta en 0
    GOTO CHECAR ;BRA NZ,CHECAR; si no esta en cero significa que aun esta ocupado
    CLR TRISB; se vuelve a poner PORTB como salida
    BCLR PORTD, #RW_LCD
    NOP

    return
    
_iniLCD8bits:
    CALL _RETARDO_15ms
    MOV #0x30, W0
    CALL   _comandoLCD

    CALL _RETARDO_15ms
    MOV #0x30, W0
    CALL   _comandoLCD

    CALL _RETARDO_15ms
    MOV #0x30, W0
    CALL   _comandoLCD

    CALL _busyflagLCD
    MOV #0x38, W0
    CALL   _comandoLCD
    
    CALL _busyflagLCD
    MOV #0x08, W0
    CALL   _comandoLCD

    CALL _busyflagLCD
    MOV #0x01, W0
    CALL   _comandoLCD    
    
    CALL _busyflagLCD
    MOV #0x06, W0
    CALL   _comandoLCD    

    CALL _busyflagLCD
    MOV #0x0F, W0
    CALL   _comandoLCD    
    CALL _busyflagLCD
    ;done
    
    return
    
_comandoLCD:
    BCLR PORTD, #RS_LCD
    NOP
    BCLR PORTD, #RW_LCD
    NOP
    BSET PORTD, #E_LCD
    NOP
    MOV W0, PORTB ; MOV.B W0, PORTB
    NOP
    BCLR PORTD, #E_LCD
    NOP
    
    return
    
  
    
    
    
_RETARDO_15ms:
    PUSH W0
    CLR W0
    MOV #0x24E1,W0
CICLO1:
    DEC W0,W0
    BRA NZ,CICLO1

    POP W0
    RETURN

    
    

    .include "p30F4013.inc"
    .GLOBAL __T1Interrupt
    .GLOBAL _useg
    .GLOBAL _dseg
    .GLOBAL _umin
    .GLOBAL _dmin
    .GLOBAL _uhr
    .GLOBAL _dhr
    .GLOBAL _incrementa


    
    
__T1Interrupt:
    PUSH W0
    INC _useg
    MOV #10,W0
    CP.B _useg
    BRA NZ,FIN
    ;si _useg es igual a 10 se llega al codigo de abajo (bandera z)
    CLR.B _useg
    INC.B _dseg
    MOV #6,W0
    CP.B _dseg
    BRA NZ, FIN
    ;si _dseg es igual a 6 se llega al codigo de abajo (bandera z)
    CLR.B _dseg
    INC.B _umin
    MOV #10,W0
    CP.B _umin
    BRA NZ, FIN
    ;si _umin es igual a 10 se llega al codigo de abajo (bandera z)
    CLR.B _umin
    INC.B _dmin
    MOV #6,W0
    CP.B _dmin
    BRA NZ, FIN
    ;si _dmin es igual a 6 se llega al codigo de abajo  (bandera z)
    
    CLR.B _dmin
    INC.B _uhrd
    MOV #1, W0
    CP.B _dhr
    BRA GT, HORA_DOS; si _uhr es mayor a 1 
    ;si _uhr es igual a 1 se llega al codigo de abajo 
    MOV #10,W0
    CP.B _uhr
    BRA NZ,FIN
    ;si es igual se borra uhr y se incrementa _dhr
    CLR.B _uhr
    INC.B _dhr
    GOTO FIN

HORA_DOS:
    MOV #2,W0
    CP.B _dhr
    BRA NZ, FIN
    ;si _dhr es igual a dos se llega al codigo de abajo
    MOV #4, W0
    CP.B _uhr
    BRA NZ,FIN
    ;si _uhr es igual a 5 se llega al codigo de abajo
    CLR.B _uhr
    CLR.B _dhr
    GOTO FIN
    

FIN:
    BCLR IFS0,#T1IF
    POP W0
    retfie
    
    
    

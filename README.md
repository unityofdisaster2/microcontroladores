# microcontroladores

Repositorio de los códigos desarrollados para la materia de microcontroladores de ESCOM
Se utiliza el microcontrolador DSPIC30F4013
contenido del repositorio:

* practicas_introductorias. Contiene los siguientes archivos:
  * pr1Puertos.s En este archivo se encuentra un ejemplo básico sobre el direccionamiento de puertos
  * pr2SR.s En este archivo se encuentra un archivo donde se suma o resta un valor constante dependiendo lo que se encuentre en RF0
  * pr3Contador En este archivo se encuentra un ejemplo de un contador ascendente y descendente
  * PRINCIPAL4B.s En este archivo se implementa un código que muestra un digito de la boleta de un alumno a partir de un dip switch
  * PRINCIPAL5B.s En este archivo se implementa un código que muestra la boleta de un alumno en un display de forma automatica
* # cembebido.X.
  * En esta carpeta se encuentra el archivo LCD donde se realiza la configuración de este dispositivo y su uso para mostrar mensajes. En C se encuentra la configuración de puertos del micro y un hola mundo.
* # INTEXT.X.
  * En esta carpeta se encuentra un ejemplo del uso de interrupciones externas. Se utiliza un sensor hall para activar las interrupciones que haran que se aumente un contador que es mostrado en el LCD.
* # PIANO.X
  * En esta carpeta se encuentra un ejemplo sobre uso de interrupciones internas y el uso del oscilador. Se conectan varios
  * botones al puerto F y cada que se presionen se reconfigurara la frecuencia del oscilador para que arroje una señal correspondientes a las notas de un piano.  
* # Reloj.X
  * Contiene la implementacion en C y ensamblador de un reloj en tiempo real que utiliza como apoyo un cristal oscilador externo de 32KHZ. Se utiliza la "libreria" de LCD para mostrar en este dispositivo el conteo del reloj. Se configura la interrupción para que en cada llamado se aumenten los contadores de segundos, minutos y horas cuando sea necesario.
* # UART.X
  * Contiene un ejemplo de como utilizar el UART del microcontrolador con el modulo FT232 serial para poder enviar un mensaje desde la computadora al micro y este se muestre en un LCD.
* # ModuloWIFI
  * Contiene un ejemplo de uso de UART de microcontrolador con modulo serial FT232 y modulo WIFI ESP8266. Se configura modulo WIFI mediante comandos AT y se crea una conexion con un servidor para enviar un mensaje desde el micro. El modulo FT232 se encargara de mostrar en consola todas las respuestas del modulo WIFI al momento de realizar configuraciones.
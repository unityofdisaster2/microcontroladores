import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;



/**
 * Cliente
 */
public class Cliente {
    public static void main(String[] args) throws Exception{
        DataOutputStream dos;
        Scanner sc = new Scanner(System.in);
        String mensaje = sc.nextLine();

        Socket cl = new Socket("10.0.0.7", 9000);
        dos = new DataOutputStream(cl.getOutputStream());
        byte[] b =  new byte[mensaje.length()];
        b = mensaje.getBytes();
        
        dos.write(b);
        dos.close();
        cl.close();
    }
    
}
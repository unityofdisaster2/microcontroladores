
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Servidor
 */
public class Servidor {

    public static void main(String[] args) throws Exception {
        ServerSocket s = new ServerSocket(9000, 5, InetAddress.getByName("10.0.0.7"));
        DataInputStream dis;
        DataOutputStream dos;
        Socket cl;
        for (;;) {
            cl = s.accept();
            dis = new DataInputStream(cl.getInputStream());
            dos = new DataOutputStream(cl.getOutputStream());

            byte[] b = new byte[1024];
            dis.read(b);
            String mensaje = new String(b, 0, b.length);
            System.out.println("recibido:" + mensaje);
            String msjServidor = "Mensaje enviado de servidor\n";
            dos.write(msjServidor.getBytes());
            dis.close();
            dos.close();
            cl.close();
        }

    }
}
.include "p30F4013.inc"
.GLOBAL __U1RXInterrupt

.GLOBAL _RETARDO1S
.GLOBAL _iniciarWIFI
.GLOBAL _comando

__U1RXInterrupt:
    PUSH W0
    MOV U1RXREG,W0
    MOV W0,U2TXREG
    BCLR  IFS0, #U1RXIF 
    POP W0
    retfie

_comando:
    PUSH W1
    inicio_comando:
	MOV.B [W0++],W1
	CP0.B W1
	BRA Z,fin_comando
	BCLR IFS0,#U1TXIF
	MOV W1,U1TXREG
	espera_interrupcion:
	    BTSS IFS0,#U1TXIF
	    GOTO espera_interrupcion
	    GOTO inicio_comando
	
    fin_comando:
	POP W1
	return
    
    
;_comando:
;    PUSH W1
;inicioComando:
;    MOV [W0++],W1
;    CP0 W1
;    BRA Z, fin ;si es 0, se va a fin
;    BCLR IFS0,#U1TXIF
;    MOV W1,U1TXREG
;pooling:
;    MOV U1TXIF,W1
;    CP W1,#1
;    BRA NZ, pooling ;si no es uno va a pooling
;    GOTO inicioComando
;    fin:
;    POP W1
;    return
    



_iniciarWIFI:
    BSET PORTA, #RA11
    CALL _RETARDO1S
    BSET PORTD, #RD0
    CALL _RETARDO1S
    BCLR PORTD, #RD0
    CALL _RETARDO1S
    BSET PORTD, #RD0
    CALL _RETARDO1S
    return

    
_RETARDO1S:
	PUSH	W0  ; PUSH.D W0
	PUSH	W1
	
	MOV	#10,	    W1
CICLO2_1S:
    
	CLR	W0	
CICLO1_1S:	
	DEC	W0,	    W0
	BRA	NZ,	    CICLO1_1S	
    
	DEC	W1,	    W1
	BRA	NZ,	    CICLO2_1S
	
	POP	W1  ; POP.D W0
	POP	W0
	RETURN

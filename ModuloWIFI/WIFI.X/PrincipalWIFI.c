/**@brief: Este programa muestra los bloques de un 
 * programa en C embebido para el DSPIC, los bloques son:
 * BLOQUE 1. OPCIONES DE CONFIGURACION DEL DSC: OSCILADOR, WATCHDOG,
 * BROWN OUT RESET, POWER ON RESET Y CODIGO DE PROTECCION
 * BLOQUE 2. EQUIVALENCIAS Y DECLARACIONES GLOBALES
 * BLOQUE 3. ESPACIOS DE MEMORIA: PROGRAMA, DATOS X, DATOS Y, DATOS NEAR
 * BLOQUE 4. CÓDIGO DE APLICACIÓN
 * @device: DSPIC30F4013
 * @oscillator: FRC, 7.3728MHz
 */
#include "p30F4013.h"
/********************************************************************************/
/* 						BITS DE CONFIGURACIÓN									*/
/********************************************************************************/
/* SE DESACTIVA EL CLOCK SWITCHING Y EL FAIL-SAFE CLOCK MONITOR (FSCM) Y SE 	*/
/* ACTIVA EL OSCILADOR INTERNO (FAST RC) PARA TRABAJAR							*/
/* FSCM: PERMITE AL DISPOSITIVO CONTINUAR OPERANDO AUN CUANDO OCURRA UNA FALLA 	*/
/* EN EL OSCILADOR. CUANDO OCURRE UNA FALLA EN EL OSCILADOR SE GENERA UNA 		*/
/* TRAMPA Y SE CAMBIA EL RELOJ AL OSCILADOR FRC  								*/
/********************************************************************************/
//_FOSC(CSW_FSCM_OFF & FRC); 
#pragma config FOSFPR = FRC             // Oscillator (Internal Fast RC (No change to Primary Osc Mode bits))
#pragma config FCKSMEN = CSW_FSCM_OFF   // Clock Switching and Monitor (Sw Disabled, Mon Disabled)/********************************************************************************/
/* SE DESACTIVA EL WATCHDOG														*/
/********************************************************************************/
//_FWDT(WDT_OFF); 
#pragma config WDT = WDT_OFF            // Watchdog Timer (Disabled)
/********************************************************************************/
/* SE ACTIVA EL POWER ON RESET (POR), BROWN OUT RESET (BOR), 					*/
/* POWER UP TIMER (PWRT) Y EL MASTER CLEAR (MCLR)								*/
/* POR: AL MOMENTO DE ALIMENTAR EL DSPIC OCURRE UN RESET CUANDO EL VOLTAJE DE 	*/
/* ALIMENTACIÓN ALCANZA UN VOLTAJE DE UMBRAL (VPOR), EL CUAL ES 1.85V			*/
/* BOR: ESTE MODULO GENERA UN RESET CUANDO EL VOLTAJE DE ALIMENTACIÓN DECAE		*/
/* POR DEBAJO DE UN CIERTO UMBRAL ESTABLECIDO (2.7V) 							*/
/* PWRT: MANTIENE AL DSPIC EN RESET POR UN CIERTO TIEMPO ESTABLECIDO, ESTO 		*/
/* AYUDA A ASEGURAR QUE EL VOLTAJE DE ALIMENTACIÓN SE HA ESTABILIZADO (16ms) 	*/
/********************************************************************************/
//_FBORPOR( PBOR_ON & BORV27 & PWRT_16 & MCLR_EN ); 
// FBORPOR
#pragma config FPWRT  = PWRT_16          // POR Timer Value (16ms)
#pragma config BODENV = BORV20           // Brown Out Voltage (2.7V)
#pragma config BOREN  = PBOR_ON          // PBOR Enable (Enabled)
#pragma config MCLRE  = MCLR_EN          // Master Clear Enable (Enabled)
/********************************************************************************/
/*SE DESACTIVA EL CÓDIGO DE PROTECCIÓN											*/
/********************************************************************************/
//_FGS(CODE_PROT_OFF);      
// FGS
#pragma config GWRP = GWRP_OFF          // General Code Segment Write Protect (Disabled)
#pragma config GCP = CODE_PROT_OFF      // General Segment Code Protection (Disabled)

/********************************************************************************/
/* SECCIÓN DE DECLARACIÓN DE CONSTANTES CON DEFINE								*/
/********************************************************************************/
#define EVER 1
#define MUESTRAS 64

/********************************************************************************/
/* DECLARACIONES GLOBALES														*/
/********************************************************************************/
/*DECLARACIÓN DE LA ISR DEL TIMER 1 USANDO __attribute__						*/
/********************************************************************************/
void __attribute__((__interrupt__)) _T1Interrupt(void);

/********************************************************************************/
/* CONSTANTES ALMACENADAS EN EL ESPACIO DE LA MEMORIA DE PROGRAMA				*/
/********************************************************************************/
int ps_coeff __attribute__((aligned(2), space(prog)));
/********************************************************************************/
/* VARIABLES NO INICIALIZADAS EN EL ESPACIO X DE LA MEMORIA DE DATOS			*/
/********************************************************************************/
int x_input[MUESTRAS] __attribute__((space(xmemory)));
/********************************************************************************/
/* VARIABLES NO INICIALIZADAS EN EL ESPACIO Y DE LA MEMORIA DE DATOS			*/
/********************************************************************************/
int y_input[MUESTRAS] __attribute__((space(ymemory)));
/********************************************************************************/
/* VARIABLES NO INICIALIZADAS LA MEMORIA DE DATOS CERCANA (NEAR), LOCALIZADA	*/
/* EN LOS PRIMEROS 8KB DE RAM													*/
/********************************************************************************/
int var1 __attribute__((near));

void iniPerifericos(void);
void iniInterrupciones(void);
void iniUART1(void);
void iniUART2(void);
void enableUARTs(void);
void iniciarWIFI(void);
void configurarWIFI(void);
void RETARDO1S(void);
void cerrarWIFI(void);
void comando(unsigned char *);



//directiva static tiene la misma funcion que en java

//directiva register funciona para colocar variables en registros internos del micro

//directiva extern para compartir una variable entre distintos programas
//se declara de forma normal en un programa, posteriormente si se quiere utilizar
//en otros se utiliza extern


//paso de parametros a traves de registros o a traves de la pila
/*los argumentos se van guardando en orden en los registros para los
primeros 8 parametros*/
//si se envia del nueve en adelante estos ya se empiezan a guardar en la pila

unsigned char cmdRST[] ="AT+RST\r\n";
unsigned char cmdCWMODE[] ="AT+CWMODE=1\r\n";
unsigned char cmdCIPMUX[] ="AT+CIPMUX=0\r\n";
//SSID del modem y contrasena
//unsigned char cmdCWJAP[] ="AT+CWJAP=\"HOME-F3D6\",\"165233DACA81D3B9\"\r\n";
//unsigned char cmdCWJAP[] ="AT+CWJAP=\"Tenda_80AFF0\",\"\"\r\n";
unsigned char cmdCWJAP[] ="AT+CWJAP=\"red_ray\",\"12345678\"\r\n";
unsigned char cmdCIFSR[] ="AT+CIFSR\r\n";
//Direccion ip y el puerto
//unsigned char cmdCIPSTART[] ="AT+CIPSTART=\"TCP\",\"192.168.0.101\",9000\r\n";
unsigned char cmdCIPSTART[] ="AT+CIPSTART=\"TCP\",\"192.168.43.6\",9000\r\n";
unsigned char cmdCIPMODE[] ="AT+CIPMODE=1\r\n";
unsigned char cmdCIPSEND[] ="AT+CIPSEND\r\n";
unsigned char cmdCIPCLOSE[] ="AT+CIPCLOSE\r\n";
unsigned char cmdSTOPPT[] ="+++";










int main(void) {

    iniPerifericos();
    iniUART1();
    iniUART2();
    iniInterrupciones();
    enableUARTs();
    iniciarWIFI();
    configurarWIFI(); //completar
    
    U1TXREG = 'H';
    U1TXREG = 'O';
    U1TXREG = 'L';
    U1TXREG = 'A';
    
    RETARDO1S();

    cerrarWIFI();  //completar

    for(;EVER;)
    {   
        Nop();
    }
    
    return 0;
}

void cerrarWIFI(void){
    comando(cmdSTOPPT);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCIPCLOSE);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
}

void configurarWIFI(){
    comando(cmdRST);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCWMODE);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCIPMUX);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCWJAP);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCIFSR);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCIPSTART);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCIPMODE);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    comando(cmdCIPSEND);
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
    RETARDO1S();
}

void iniUART1(void) {
    //se activa mecanismo
    U1MODE = 0x0000;
    Nop();
    U1STA = 0x8000;
    Nop();
    U1BRG = 0;
    Nop();
}

void iniUART2(void) {
    //se activa mecanismo
    U2MODE = 0x0000;
    Nop();
    U2STA = 0x8000;
    Nop();
    U2BRG = 0;
    Nop();
}

void enableUARTs(void) {
    //se activa mecanismo
    U1MODEbits.UARTEN = 1;
    Nop();
    U1STAbits.UTXEN = 1;
    Nop();
    U2MODEbits.UARTEN = 1;
    Nop();
    U2STAbits.UTXEN = 1;
    Nop();
}
/****************************************************************************/
/* DESCRICION:	ESTA RUTINA INICIALIZA LAS INTERRPCIONES    				*/
/* PARAMETROS: NINGUNO                                                      */
/* RETORNO: NINGUNO															*/


/****************************************************************************/
void iniInterrupciones(void) {
    //se activa mecanismo
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1;
}
/****************************************************************************/
/* DESCRICION:	ESTA RUTINA INICIALIZA LOS PERIFERICOS						*/
/* PARAMETROS: NINGUNO                                                      */
/* RETORNO: NINGUNO															*/

/****************************************************************************/
void iniPerifericos(void) {
    //se configura PORTA, PORTF y PORTD como salida
    TRISA = 0;
    Nop();
    PORTA = 0;
    Nop();
    LATA = 0;
    Nop();
    
    TRISFbits.TRISF5 = 0;
    Nop();
    TRISFbits.TRISF3 = 0;
    Nop();
    TRISFbits.TRISF4 = 1;
    Nop();
    TRISFbits.TRISF2 = 1;
    Nop();
    PORTF = 0;
    Nop();
    LATF = 0;
    Nop();
    

    TRISD = 0;
    Nop();
    PORTD = 0;
    Nop();
    LATD = 0;
    Nop();
    
  
    ADPCFG = 0xFFFF;
    Nop();       
}

/********************************************************************************/
/* DESCRICION:	ISR (INTERRUPT SERVICE ROUTINE) DEL TIMER 1						*/
/* LA RUTINA TIENE QUE SER GLOBAL PARA SER UNA ISR								*/
/* SE USA PUSH.S PARA GUARDAR LOS REGISTROS W0, W1, W2, W3, C, Z, N Y DC EN LOS */
/* REGISTROS SOMBRA																*/

/********************************************************************************/
/*
void __attribute__((__interrupt__)) _T1Interrupt(void) {
    IFS0bits.T1IF = 0; //SE LIMPIA LA BANDERA DE INTERRUPCION DEL TIMER 1                      
}
*/
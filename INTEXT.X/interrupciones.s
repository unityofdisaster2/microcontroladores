    .include "p30F4013.inc"
    .GLOBAL __INT0Interrupt
    .GLOBAL _uni
    .GLOBAL _dece
    .GLOBAL _cent
    .GLOBAL _umi

    
__INT0Interrupt:
    inc.b _uni 
    mov #10, w0
    cp.b _uni
    bra nz, fin
    
    clr.b _uni
    inc.b _dece
    cp.b _dece
    bra nz,fin
    
    clr.b _dece
    inc.b _cent
    cp.b _cent
    bra nz,fin
    
    clr.b _cent
    inc.b _umi
    cp.b _umi
    bra nz,fin
    
fin:
    bclr IFS0, #INT0IF
    
    retfie
    
    
    
    

